# Defold Template

The template for Defold projects with Haxe language support through [hxdefold](https://github.com/hxdefold/hxdefold).

Prepared with a `gitlab-ci.yml` which builds the game for all targets, and publishes the HTML5 build on GitLab Pages.
