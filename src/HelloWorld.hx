import defold.support.Script;


typedef HelloWorldProperties =
{

}


class HelloWorld extends Script<HelloWorldProperties>
{
    override function init(self: HelloWorldProperties)
    {
        trace('Hello world!');
    }
}
